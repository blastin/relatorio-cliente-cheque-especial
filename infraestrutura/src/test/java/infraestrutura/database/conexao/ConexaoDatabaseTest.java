package infraestrutura.database.conexao;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class ConexaoDatabaseTest {

    @Test
    public void manipularUmaConexaoSimples() throws SQLException {

        final var connection = Mockito.mock(Connection.class);

        final ConexaoWrapper conexaoWrapper = () -> connection;

        final var conexaoDatabase = ConexaoDatabaseFabrica.conexaoSimples(conexaoWrapper);

        final var statementMockiado = Mockito.mock(Statement.class);

        Mockito.when(connection.createStatement()).thenReturn(statementMockiado);

        final var statement = conexaoDatabase.novoStatement();

        Assert.assertNotNull("não pode ser nulo", statement);
        Assert.assertEquals("statement deve ser o mockiado", statementMockiado, statement);

        conexaoDatabase.desconectar();

    }

    @Test
    public void manipularUmaConexaoConcorrente() throws SQLException {

        final var connection = Mockito.mock(Connection.class);

        final ConexaoWrapper conexaoWrapper = () -> connection;

        final var conexaoDatabase = ConexaoDatabaseFabrica.conexaoConcorrente(conexaoWrapper);

        final var statementMockiado = Mockito.mock(Statement.class);

        Mockito.when(connection.createStatement()).thenReturn(statementMockiado);

        final var statement = conexaoDatabase.novoStatement();

        Assert.assertNotNull("não pode ser nulo", statement);

        Assert.assertEquals("statement deve ser o mockiado", statementMockiado, statement);

        conexaoDatabase.desconectar();

    }
}
