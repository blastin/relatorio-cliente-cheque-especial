package infraestrutura.database;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.stream.Stream;

public class BancoDeDadosComponenteTest {

    @Test
    public void consultandoBancoDeDados() throws SQLException, BancoDeDadosException {

        final var query = "select * from cliente c where c.id = 1";

        final var indiceResultado = 1;

        final var resultado = "resultado";

        final var connection = Mockito.mock(Connection.class);

        final var statementMockiado = Mockito.mock(Statement.class);

        Mockito.when(connection.createStatement()).thenReturn(statementMockiado);

        final var resultSetMockiado = Mockito.mock(ResultSet.class);

        Mockito.when(resultSetMockiado.next()).thenReturn(true).thenReturn(false);

        Mockito.when(resultSetMockiado.getString(indiceResultado)).thenReturn(resultado);

        Mockito.when(statementMockiado.executeQuery(query)).thenReturn(resultSetMockiado);

        final var bancoDeDadosComponente = BancoDeDadosComponenteFabrica.construir(() -> connection);

        final Stream<String> stream = bancoDeDadosComponente
                .selecionar(query, resultSet -> resultSet.getString(1));

        final String resultadoRecebido = stream.findFirst().orElse("");

        Assert.assertEquals("deve ser igual", resultado, resultadoRecebido);

        Mockito.verify(connection).createStatement();

        Mockito.verify(statementMockiado).executeQuery(query);

        Mockito.verify(resultSetMockiado).getString(indiceResultado);

    }


    @Test(expected = BancoDeDadosException.class)
    public void gerandoSQLExceptionEmStatement() throws SQLException, BancoDeDadosException {

        final var connection = Mockito.mock(Connection.class);

        Mockito.when(connection.createStatement()).thenThrow(new SQLException(""));

        final var bancoDeDadosComponente = BancoDeDadosComponenteFabrica.construir(() -> connection);

        bancoDeDadosComponente.selecionar("query", ResultSet::isClosed);

        Mockito.verify(connection).createStatement();

    }

    @Test(expected = BancoDeDadosException.class)
    public void gerandoSQLExceptionEmDesconexao() throws SQLException, BancoDeDadosException {

        final var connection = Mockito.mock(Connection.class);

        final var statementMockiado = Mockito.mock(Statement.class);

        Mockito.when(connection.createStatement()).thenReturn(statementMockiado);

        final var resultSetMockiado = Mockito.mock(ResultSet.class);

        Mockito.when(resultSetMockiado.next()).thenReturn(true).thenReturn(false);

        Mockito.when(statementMockiado.executeQuery(Mockito.anyString())).thenReturn(resultSetMockiado);

        Mockito.doThrow(new SQLException("")).when(connection).close();

        final var bancoDeDadosComponente = BancoDeDadosComponenteFabrica.construir(() -> connection);

        bancoDeDadosComponente.selecionar("query", resultSet -> {
                    resultSet.close();
                    return resultSet.isClosed();

                }
        );

    }
}
