package infraestrutura.database.stream;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.ResultSet;
import java.util.stream.Stream;

public class ResultSetStreamTest {

    @Test
    public void mapeamentoDeDados() throws Exception {

        final var resultSet = Mockito.mock(ResultSet.class);

        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);

        Mockito.when(resultSet.isClosed()).thenReturn(true);

        final Stream<Boolean> stream = ResultSetStreamFabrica.construir(resultSet, ResultSet::isClosed);

        final Boolean aberto = stream.findFirst().orElseThrow(Exception::new);

        Assert.assertTrue("deve estar aberto", aberto);

    }

    @Test
    public void mapeamentoSemResultado() throws Exception {

        final var resultSet = Mockito.mock(ResultSet.class);

        Mockito.when(resultSet.next()).thenReturn(false);

        Mockito.when(resultSet.isClosed()).thenReturn(true);

        final Stream<Boolean> stream = ResultSetStreamFabrica.construir(resultSet, ResultSet::isClosed);

        Assert.assertEquals("stream vazio", 0, stream.count());

    }
}
