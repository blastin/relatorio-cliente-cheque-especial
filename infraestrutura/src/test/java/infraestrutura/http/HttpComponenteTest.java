package infraestrutura.http;

import org.junit.Assert;
import org.junit.Test;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSession;
import java.io.IOException;
import java.net.Authenticator;
import java.net.CookieHandler;
import java.net.ProxySelector;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpHeaders;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

public class HttpComponenteTest {

    @Test
    public void requisicaoRest() throws HttpComponenteException, IOException, InterruptedException {

        final var status = 200;

        final var corpo = "valorRetornado";

        final var responseParaTeste = new ResponseParaTeste(status, corpo);

        final var httpClient = new HttpClienteParaTeste(responseParaTeste);

        final var httpComponente = HttpComponenteFabrica.construir(httpClient);

        final var httpResposta = httpComponente.verboGet("http://HttpComponenteTest:0/requisicaoRest");

        Assert.assertNotNull("resposta não pode ser nulo", httpResposta);

        Assert.assertTrue("deve retornar sucesso", httpResposta.status2xx());

        Assert.assertEquals("status deve ser 200", status, httpResposta.status());

        Assert.assertEquals("corpo deve ser igual", corpo, httpResposta.corpo());

    }

    @Test(expected = HttpComponenteException.class)
    public void requisicaoRestComRetornoDeExcessao() throws HttpComponenteException, IOException, InterruptedException {

        final var status = 200;

        final var corpo = "valorRetornado";

        final var responseParaTeste = new ResponseParaTeste(status, corpo);

        final var httpClient = new HttpClienteParaTeste(responseParaTeste);

        final var httpComponente = new HttpComponenteImpl(httpClient);

        httpComponente.verboGet("http://HttpComponenteTest:0/requisicaoRest");

    }

    private static class HttpClienteParaTeste extends HttpClient {

        private final HttpResponse<String> responseParaTeste;

        private HttpClienteParaTeste(HttpResponse<String> responseParaTeste) {
            this.responseParaTeste = responseParaTeste;
        }

        @Override
        public Optional<CookieHandler> cookieHandler() {
            return Optional.empty();
        }

        @Override
        public Optional<Duration> connectTimeout() {
            return Optional.empty();
        }

        @Override
        public Redirect followRedirects() {
            return null;
        }

        @Override
        public Optional<ProxySelector> proxy() {
            return Optional.empty();
        }

        @Override
        public SSLContext sslContext() {
            return null;
        }

        @Override
        public SSLParameters sslParameters() {
            return null;
        }

        @Override
        public Optional<Authenticator> authenticator() {
            return Optional.empty();
        }

        @Override
        public Version version() {
            return null;
        }

        @Override
        public Optional<Executor> executor() {
            return Optional.empty();
        }

        @SuppressWarnings("unchecked")
        @Override
        public <T> HttpResponse<T> send(HttpRequest httpRequest, HttpResponse.BodyHandler<T> bodyHandler) {
            return (HttpResponse<T>) responseParaTeste;
        }

        @Override
        public <T> CompletableFuture<HttpResponse<T>> sendAsync(HttpRequest httpRequest, HttpResponse.BodyHandler<T> bodyHandler) {
            return null;
        }

        @Override
        public <T> CompletableFuture<HttpResponse<T>> sendAsync(HttpRequest httpRequest, HttpResponse.BodyHandler<T> bodyHandler, HttpResponse.PushPromiseHandler<T> pushPromiseHandler) {
            return null;
        }
    }


    private static class ResponseParaTeste implements HttpResponse<String> {

        private final int status;

        private final String corpo;

        private ResponseParaTeste(int status, String corpo) {
            this.status = status;
            this.corpo = corpo;
        }

        @Override
        public int statusCode() {
            return status;
        }

        @Override
        public HttpRequest request() {
            return null;
        }

        @Override
        public Optional<HttpResponse<String>> previousResponse() {
            return Optional.empty();
        }

        @Override
        public HttpHeaders headers() {
            return null;
        }

        @Override
        public String body() {
            return corpo;
        }

        @Override
        public Optional<SSLSession> sslSession() {
            return Optional.empty();
        }

        @Override
        public URI uri() {
            return null;
        }

        @Override
        public HttpClient.Version version() {
            return null;
        }
    }
}
