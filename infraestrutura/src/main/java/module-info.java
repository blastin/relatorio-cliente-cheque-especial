module infraestrutura {
    exports infraestrutura.database;
    exports infraestrutura.http;
    exports infraestrutura.database.conexao;
    exports infraestrutura.database.stream;
    requires transitive java.net.http;
    requires transitive java.sql;
    requires org.slf4j;
}
