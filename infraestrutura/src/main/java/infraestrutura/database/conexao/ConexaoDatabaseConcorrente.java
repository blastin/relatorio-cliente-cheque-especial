package infraestrutura.database.conexao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

final class ConexaoDatabaseConcorrente implements ConexaoDatabase {

    private final ConexaoWrapper conexaoWrapper;

    private final Map<Long, ConexaoPool> conexoes;

    ConexaoDatabaseConcorrente(final ConexaoWrapper conexaoWrapper) {

        this.conexaoWrapper = conexaoWrapper;

        conexoes = new ConcurrentHashMap<>();

    }

    @Override
    public Statement novoStatement() throws SQLException {

        final var currentThread = Thread.currentThread();

        final var id = currentThread.getId();

        if (!conexoes.containsKey(id)) {
            conexoes.put(id, new ConexaoPool(conexaoWrapper));
        }

        return conexoes.get(id).statement;

    }

    @Override
    public void desconectar() throws SQLException {

        final var currentThread = Thread.currentThread();

        final var id = currentThread.getId();

        final var conexaoPool = conexoes.get(id);

        conexaoPool.fechar();

    }

    private static class ConexaoPool {

        private final Connection conexaoSQL;

        private final Statement statement;

        private ConexaoPool(final ConexaoWrapper conexaoWrapper) throws SQLException {

            this.conexaoSQL = conexaoWrapper.novaConexao();

            this.statement = conexaoSQL.createStatement();

        }

        private void fechar() throws SQLException {

            statement.close();

            conexaoSQL.close();

        }
    }
}
