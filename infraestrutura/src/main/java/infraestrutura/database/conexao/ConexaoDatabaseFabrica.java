package infraestrutura.database.conexao;

public final class ConexaoDatabaseFabrica {

    private ConexaoDatabaseFabrica() {
    }

    public static ConexaoDatabase conexaoConcorrente(final ConexaoWrapper conexaoWrapper) {
        return new ConexaoDatabaseConcorrente(conexaoWrapper);
    }

    public static ConexaoDatabase conexaoSimples(final ConexaoWrapper conexaoWrapper) {
        return new ConexaoDatabaseSimples(conexaoWrapper);
    }

}
