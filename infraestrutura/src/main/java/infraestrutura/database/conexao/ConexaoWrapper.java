package infraestrutura.database.conexao;

import java.sql.Connection;
import java.sql.SQLException;

@FunctionalInterface
public interface ConexaoWrapper {

    Connection novaConexao() throws SQLException;

}
