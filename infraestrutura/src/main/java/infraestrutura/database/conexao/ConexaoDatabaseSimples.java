package infraestrutura.database.conexao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

final class ConexaoDatabaseSimples implements ConexaoDatabase {

    private final ConexaoWrapper conexaoWrapper;

    private Connection conexaoSQL;

    private Statement statement;

    ConexaoDatabaseSimples(final ConexaoWrapper conexaoWrapper) {
        this.conexaoWrapper = conexaoWrapper;
    }

    @Override
    public Statement novoStatement() throws SQLException {

        conexaoSQL = conexaoWrapper.novaConexao();

        statement = conexaoSQL.createStatement();

        return statement;

    }

    @Override
    public void desconectar() throws SQLException {

        statement.close();

        conexaoSQL.close();

    }

}
