package infraestrutura.database.conexao;

import java.sql.SQLException;
import java.sql.Statement;

public interface ConexaoDatabase {

    Statement novoStatement() throws SQLException;

    void desconectar() throws SQLException;

}
