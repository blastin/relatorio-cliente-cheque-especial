package infraestrutura.database;

public final class BancoDeDadosException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 8089545177015807006L;

    BancoDeDadosException(final String mensagem) {
        super(mensagem);
    }
}
