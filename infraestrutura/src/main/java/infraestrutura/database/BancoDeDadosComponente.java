package infraestrutura.database;

import infraestrutura.database.stream.Funcao;

import java.sql.ResultSet;
import java.util.stream.Stream;

@FunctionalInterface
public interface BancoDeDadosComponente {

    <E> Stream<E> selecionar(final String query, final Funcao<ResultSet, ? extends E> funcao) throws BancoDeDadosException;

}
