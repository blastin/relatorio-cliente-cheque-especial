package infraestrutura.database.stream;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Stream;

final class ResultSetStreamImpl<E> implements ResultSetStream<E> {

    private final ResultSet resultSet;

    ResultSetStreamImpl(final ResultSet resultSet) {
        this.resultSet = resultSet;
    }

    @Override
    public Stream<E> map(final Funcao<ResultSet, ? extends E> funcao) throws SQLException {

        final Collection<E> colecao = new ArrayList<>();

        while (resultSet.next()) {
            colecao.add(funcao.aplicar(resultSet));
        }

        return colecao.parallelStream();

    }

}
