package infraestrutura.database.stream;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.stream.Stream;

@FunctionalInterface
public interface ResultSetStream<E> {

    Stream<E> map(Funcao<ResultSet, ? extends E> funcao) throws SQLException;

}
