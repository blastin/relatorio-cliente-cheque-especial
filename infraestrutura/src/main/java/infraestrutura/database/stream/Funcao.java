package infraestrutura.database.stream;

import java.sql.SQLException;

@FunctionalInterface
public interface Funcao<T, E> {

    E aplicar(T t) throws SQLException;

}
