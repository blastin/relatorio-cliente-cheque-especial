package infraestrutura.database.stream;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.stream.Stream;

public final class ResultSetStreamFabrica {

    private ResultSetStreamFabrica() {
    }

    public static <E> Stream<E> construir(final ResultSet resultSet, final Funcao<ResultSet, ? extends E> funcao) throws SQLException {
        return new ResultSetStreamImpl<E>(resultSet).map(funcao);
    }

}
