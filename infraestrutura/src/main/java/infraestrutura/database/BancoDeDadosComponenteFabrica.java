package infraestrutura.database;

import infraestrutura.database.conexao.ConexaoDatabase;
import infraestrutura.database.conexao.ConexaoDatabaseFabrica;
import infraestrutura.database.conexao.ConexaoWrapper;

public final class BancoDeDadosComponenteFabrica {

    private BancoDeDadosComponenteFabrica() {
    }

    public static BancoDeDadosComponente construir(final ConexaoDatabase conexaoDatabase) {
        return new BancoDeDadosComponenteImpl(conexaoDatabase);
    }

    public static BancoDeDadosComponente construir(final ConexaoWrapper conexaoWrapper) {
        return construir(ConexaoDatabaseFabrica.conexaoSimples(conexaoWrapper));
    }

}
