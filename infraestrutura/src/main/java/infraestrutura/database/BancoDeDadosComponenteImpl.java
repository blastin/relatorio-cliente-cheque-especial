package infraestrutura.database;

import infraestrutura.database.conexao.ConexaoDatabase;
import infraestrutura.database.stream.Funcao;
import infraestrutura.database.stream.ResultSetStreamFabrica;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.stream.Stream;

final class BancoDeDadosComponenteImpl implements BancoDeDadosComponente {

    private final ConexaoDatabase conexaoDatabase;

    BancoDeDadosComponenteImpl(final ConexaoDatabase conexaoDatabase) {
        this.conexaoDatabase = conexaoDatabase;
    }

    @Override
    public <E> Stream<E> selecionar(final String query, final Funcao<ResultSet, ? extends E> funcao) throws BancoDeDadosException {

        Stream<E> stream;

        try (final ResultSet resultSet = conexaoDatabase.novoStatement().executeQuery(query)) {
            stream = ResultSetStreamFabrica.construir(resultSet, funcao);
        } catch (SQLException e) {
            throw new BancoDeDadosException(e.getMessage());
        }

        try {
            conexaoDatabase.desconectar();
        } catch (SQLException e) {
            throw new BancoDeDadosException(e.getMessage());
        }

        return stream;

    }

}

