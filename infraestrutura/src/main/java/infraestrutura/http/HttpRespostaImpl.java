package infraestrutura.http;

final class HttpRespostaImpl implements HttpResposta {

    private final int codigoStatus;

    private final String corpoRetornado;

    HttpRespostaImpl(final int codigoStatus, final String corpoRetornado) {
        this.codigoStatus = codigoStatus;
        this.corpoRetornado = corpoRetornado;
    }

    @Override
    public boolean status2xx() {
        return codigoStatus >= 200 && codigoStatus < 300;
    }

    @Override
    public int status() {
        return codigoStatus;
    }

    @Override
    public String corpo() {
        return corpoRetornado;
    }

}
