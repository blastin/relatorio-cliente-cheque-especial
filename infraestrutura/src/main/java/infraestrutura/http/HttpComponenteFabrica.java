package infraestrutura.http;

import java.net.http.HttpClient;

public final class HttpComponenteFabrica {

    private HttpComponenteFabrica() {

    }

    public static HttpComponente construir(final HttpClient httpClient) {
        return new HttpComponenteProxy(new HttpComponenteImpl(httpClient));
    }

}
