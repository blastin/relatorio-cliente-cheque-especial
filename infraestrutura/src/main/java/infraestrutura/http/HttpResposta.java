package infraestrutura.http;

public interface HttpResposta {

    boolean status2xx();

    int status();

    String corpo();

}
