package infraestrutura.http;

public final class HttpComponenteException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7944995235952049755L;

	HttpComponenteException(final String mensagem) {
        super(mensagem);
    }

}
