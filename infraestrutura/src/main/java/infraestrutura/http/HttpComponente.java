package infraestrutura.http;

import java.io.IOException;
import java.util.Map;

public interface HttpComponente {

    default HttpResposta verboGet(final String url) throws HttpComponenteException, IOException, InterruptedException {
        throw new HttpComponenteException("Evite utilizar uma implementação sem proxy.");
    }

    HttpResposta verboGet(final String url, final Map<String, ?> headers) throws IOException, InterruptedException;

}
