package infraestrutura.http;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Map;

final class HttpComponenteImpl implements HttpComponente {

    private static final Logger LOGGER = LoggerFactory.getLogger(HttpComponenteImpl.class);

    private final HttpClient httpClient;

    HttpComponenteImpl(final HttpClient httpClient) {
        this.httpClient = httpClient;
    }

    @Override
    public HttpResposta verboGet(final String url, final Map<String, ?> headers) throws IOException, InterruptedException {

        LOGGER.debug("Requisicao {}", url);

        final var builder = HttpRequest.newBuilder();

        builder.uri(URI.create(url));

        headers.forEach((chave, valor) -> builder.header(chave, valor.toString()));

        final var httpRequest = builder.build();

        final var httpResponse = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString(StandardCharsets.UTF_8));

        return new HttpRespostaImpl(httpResponse.statusCode(), httpResponse.body());

    }

}
