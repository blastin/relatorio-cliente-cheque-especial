package infraestrutura.http;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

final class HttpComponenteProxy implements HttpComponente {

    private final HttpComponente httpComponente;

    HttpComponenteProxy(final HttpComponente httpComponente) {
        this.httpComponente = httpComponente;
    }

    @Override
    public HttpResposta verboGet(final String url) throws IOException, InterruptedException {
        return verboGet(url, Collections.emptyMap());
    }

    @Override
    public HttpResposta verboGet(final String url, final Map<String, ?> headers) throws IOException, InterruptedException {
        return httpComponente.verboGet(url, headers);
    }

}
