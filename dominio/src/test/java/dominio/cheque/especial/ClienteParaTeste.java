package dominio.cheque.especial;

import dominio.cliente.Cliente;

import java.math.BigDecimal;

public final class ClienteParaTeste implements Cliente {

    public final int id;

    public final BigDecimal chequeEspecial;

    public ClienteParaTeste(final int id, final double chequeEspecial) {
        this.id = id;
        this.chequeEspecial = BigDecimal.valueOf(chequeEspecial);
    }

    public ClienteParaTeste(final int id, final BigDecimal chequeEspecial) {
        this.id = id;
        this.chequeEspecial = chequeEspecial;
    }

    @Override
    public int id() {
        return id;
    }

    @Override
    public BigDecimal chequeEspecial() {
        return chequeEspecial;
    }

}
