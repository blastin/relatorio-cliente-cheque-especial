package dominio.cheque.especial.relatorio;

import dominio.ArquivoUtil;
import dominio.cheque.especial.ClienteParaTeste;
import dominio.cliente.relatorio.RelatorioFabrica;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.stream.Stream;

public class RelatorioCSVChequeEspecialTest {

    @Test
    public void gerarRelatorio() throws IOException {

        final var a = new ClienteParaTeste(1, 1000);

        final var b = new ClienteParaTeste(2, 2000);

        final var c = new ClienteParaTeste(3, 3000);

        final var casoDeUsoRelatorioChequeEspecial = CasoDeUsoRelatorioChequeEspecialFabrica.construir(() -> Stream.of(a, b, c));

        final var relatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo = casoDeUsoRelatorioChequeEspecial.obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(RelatorioFabrica.csv());

        Assert.assertNotNull("relatorio nao deve ser nulo", relatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo);

        Assert.assertEquals("deve ser igual", ArquivoUtil.paraString("relatorios/relatorio-cliente.csv"), relatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo);

    }

    @Test
    public void construindoRelatorioVazio() {

        final var casoDeUsoRelatorioChequeEspecial = CasoDeUsoRelatorioChequeEspecialFabrica.construir(Stream::empty);

        final var relatorio = casoDeUsoRelatorioChequeEspecial.obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(RelatorioFabrica.csv());

        Assert.assertNotNull("relatorio nao pode ser nulo", relatorio);

        Assert.assertTrue("relatorio deve retornar em branco", relatorio.isEmpty());

    }

}
