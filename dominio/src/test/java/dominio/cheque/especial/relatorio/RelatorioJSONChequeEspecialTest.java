package dominio.cheque.especial.relatorio;

import dominio.ArquivoUtil;
import dominio.cheque.especial.ClienteParaTeste;
import dominio.cliente.relatorio.RelatorioFabrica;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.stream.Stream;

public class RelatorioJSONChequeEspecialTest {

    @Test
    public void gerarRelatorio() throws IOException {

        final var a = new ClienteParaTeste(3, 345.6);

        final var b = new ClienteParaTeste(6, 7654.77);

        final var casoDeUsoRelatorioChequeEspecial = CasoDeUsoRelatorioChequeEspecialFabrica.construir(() -> Stream.of(a, b));

        final var relatorio = casoDeUsoRelatorioChequeEspecial.obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(RelatorioFabrica.json());

        Assert.assertNotNull("relatorio nao pode ser nulo", relatorio);

        Assert.assertEquals("deve ser igual", ArquivoUtil.paraString("relatorios/relatorio.json").replaceAll("\\s+", ""), relatorio.replaceAll("\\s+", ""));

    }

    @Test
    public void construindoRelatorioVazio() throws IOException {

        final var casoDeUsoRelatorioChequeEspecial = CasoDeUsoRelatorioChequeEspecialFabrica.construir(Stream::empty);

        final var relatorio = casoDeUsoRelatorioChequeEspecial.obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(RelatorioFabrica.json());

        Assert.assertNotNull("relatorio nao pode ser nulo", relatorio);

        Assert.assertEquals("deve ser igual", ArquivoUtil.paraString("relatorios/relatorio-vazio.json").replaceAll("\\s+", ""), relatorio.replaceAll("\\s+", ""));

    }

}
