package dominio.cheque.especial.relatorio;

import dominio.ArquivoUtil;
import dominio.cheque.especial.ClienteParaTeste;
import dominio.cliente.relatorio.RelatorioFabrica;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.stream.Stream;

public class RelatorioTSVChequeEspecialTest {

    @Test
    public void gerarRelatorio() throws IOException {

        final var a = new ClienteParaTeste(1, 300.0);

        final var b = new ClienteParaTeste(2, 467.0);

        final var c = new ClienteParaTeste(10, 876.0);

        final var casoDeUsoRelatorioChequeEspecial = CasoDeUsoRelatorioChequeEspecialFabrica.construir(() -> Stream.of(a, b, c));

        final var relatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo = casoDeUsoRelatorioChequeEspecial.obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(RelatorioFabrica.tsv());

        Assert.assertNotNull("relatorio nao deve ser nulo", relatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo);

        Assert.assertEquals("deve ser igual", ArquivoUtil.paraString("relatorios/relatorio-cliente.tsv"), relatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo);

    }

    @Test
    public void construindoRelatorioVazio() {

        final var casoDeUsoRelatorioChequeEspecial = CasoDeUsoRelatorioChequeEspecialFabrica.construir(Stream::empty);

        final var relatorio = casoDeUsoRelatorioChequeEspecial.obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(RelatorioFabrica.tsv());

        Assert.assertNotNull("relatorio nao pode ser nulo", relatorio);

        Assert.assertTrue("relatorio deve retornar em branco", relatorio.isEmpty());

    }

}
