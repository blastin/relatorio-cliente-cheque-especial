package dominio.cheque.especial.relatorio;

import dominio.ArquivoUtil;
import dominio.cheque.especial.ClienteParaTeste;
import dominio.cliente.relatorio.RelatorioFabrica;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.stream.Stream;

public class RelatorioXMLChequeEspecialTest {

    @Test
    public void gerarRelatorio() throws IOException {

        final var a = new ClienteParaTeste(3, 345.6);

        final var b = new ClienteParaTeste(6, 7654.77);

        final var casoDeUsoRelatorioChequeEspecial = CasoDeUsoRelatorioChequeEspecialFabrica.construir(() -> Stream.of(a, b));

        final var relatorio = casoDeUsoRelatorioChequeEspecial.obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(RelatorioFabrica.xml());

        Assert.assertNotNull("relatorio nao pode ser nulo", relatorio);

        Assert.assertEquals("deve ser igual", ArquivoUtil.paraString("relatorios/relatorio.xml").replaceAll(" ", ""), relatorio.replaceAll(" ", ""));

    }

    @Test
    public void construindoRelatorioVazio() throws IOException {

        final var casoDeUsoRelatorioChequeEspecial = CasoDeUsoRelatorioChequeEspecialFabrica.construir(Stream::empty);

        final var relatorio = casoDeUsoRelatorioChequeEspecial.obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(RelatorioFabrica.xml());

        Assert.assertNotNull("relatorio nao pode ser nulo", relatorio);

        Assert.assertEquals("deve ser igual", ArquivoUtil.paraString("relatorios/relatorio-vazio.xml").replaceAll(" ", ""), relatorio.replaceAll(" ", ""));

    }

}
