package dominio.cheque.especial;

import dominio.cliente.Cliente;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ChequeEspecialTest {

    private final int id = 1;
    private final BigDecimal salarioMinimo = BigDecimal.valueOf(1000.0);

    @Test
    public void clientesComChequeEspecialAcimaSalarioMinimo() {

        final var chequeEspecial = salarioMinimo.add(BigDecimal.ONE);

        final var casoDeUsoChequeEspecial =
                CasoDeUsoChequeEspecialFabrica.construir(
                        () -> Stream.of(construirCliente(chequeEspecial)),
                        () -> () -> salarioMinimo
                );

        final var clienteStream = casoDeUsoChequeEspecial.obterClientesComChequeEspecialAcimalDoSalarioMinimo();

        final Collection<Cliente> clientes = clienteStream.collect(Collectors.toUnmodifiableList());

        Assert.assertNotNull("coleção de clienter não deve ser nulo", clientes);

        Assert.assertEquals("coleção de clienter deve retornar apenas 1", 1, clientes.size());

        final var cliente = clientes.iterator().next();

        Assert.assertSame("org.desafio.cliente retornado deve ter id igual 1", id, cliente.id());

        Assert.assertSame("org.desafio.cliente retornado deve ter chequeEspecial igual", chequeEspecial, cliente.chequeEspecial());

    }


    @Test
    public void clientesComChequeEspecialIgualSalarioMinimo() {

        final var casoDeUsoChequeEspecial =
                CasoDeUsoChequeEspecialFabrica.construir(
                        () -> Stream.of(construirCliente(salarioMinimo)),
                        () -> () -> salarioMinimo
                );

        final var clientes = casoDeUsoChequeEspecial.obterClientesComChequeEspecialAcimalDoSalarioMinimo().collect(Collectors.toUnmodifiableList());

        Assert.assertNotNull("coleção de clienter não deve ser nulo", clientes);

        Assert.assertTrue("coleção de org.desafio.cliente deve estar vazia", clientes.isEmpty());

    }

    @Test
    public void clientesComChequeEspecialAbaixoSalarioMinimo() {

        final var casoDeUsoChequeEspecial =
                CasoDeUsoChequeEspecialFabrica.construir(
                        () -> Stream.of(construirCliente(salarioMinimo.subtract(BigDecimal.TEN))),
                        () -> () -> salarioMinimo
                );

        final var clientes = casoDeUsoChequeEspecial.obterClientesComChequeEspecialAcimalDoSalarioMinimo().collect(Collectors.toUnmodifiableList());

        Assert.assertNotNull("coleção de clienter não deve ser nulo", clientes);

        Assert.assertTrue("coleção de org.desafio.cliente deve estar vazia", clientes.isEmpty());
    }

    private Cliente construirCliente(final BigDecimal chequeEspecial) {
        return new ClienteParaTeste(id, chequeEspecial);
    }

}
