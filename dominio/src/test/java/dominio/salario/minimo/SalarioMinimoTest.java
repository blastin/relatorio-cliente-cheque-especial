package dominio.salario.minimo;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicInteger;

public class SalarioMinimoTest {

    @Test
    public void corrigirSalarioMinimo() {

        final SalarioMinimoGateway salarioMinimoGateway = SalarioMinimoFabrica.construir(
                () -> () -> BigDecimal.valueOf(100),
                () -> () -> BigDecimal.valueOf(10.0)
        );

        final var salarioMinimo = salarioMinimoGateway.salarioMinimo();

        Assert.assertNotNull("salario minimo não pode ser nulo", salarioMinimo);

        Assert.assertEquals("deve retornar o valor de", BigDecimal.valueOf(90.0), salarioMinimo.valor());

    }

    @Test
    public void corrigirSalarioMinimoEmTempoReal() {

        final var correcaoReal = new AtomicInteger(1);

        final var salarioMinimoGateway = SalarioMinimoFabrica.construirParaTempoReal(
                () -> () -> BigDecimal.valueOf(100),
                () -> () -> BigDecimal.valueOf(10.0).multiply(BigDecimal.valueOf(correcaoReal.getAndIncrement()))
        );

        final var salarioMinimo = salarioMinimoGateway.salarioMinimo();

        Assert.assertNotNull("salario minimo não pode ser nulo", salarioMinimo);

        Assert.assertEquals("deve retornar o valor de", BigDecimal.valueOf(90.0), salarioMinimo.valor());

        final var salarioMinimoAposCorrecao = salarioMinimoGateway.salarioMinimo();

        Assert.assertNotNull("salario minimo não pode ser nulo", salarioMinimoAposCorrecao);

        Assert.assertEquals("deve retornar o valor de", BigDecimal.valueOf(80.0), salarioMinimoAposCorrecao.valor());

    }

}
