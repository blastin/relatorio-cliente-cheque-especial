package dominio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public final class ArquivoUtil {

    private ArquivoUtil() {
    }

    public static String paraString(final String pathFile) throws IOException {
        return Files.readString(Paths.get(Objects.requireNonNull(ArquivoUtil.class.getClassLoader().getResource(pathFile)).getPath()));
    }

}
