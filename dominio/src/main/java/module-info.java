module dominio {
    exports dominio.cliente;
    exports dominio.cheque.especial;
    exports dominio.cheque.especial.relatorio;
    exports dominio.cliente.relatorio;
    exports dominio.salario.minimo;
    exports dominio.relatorio;
    requires transitive com.google.gson;
}
