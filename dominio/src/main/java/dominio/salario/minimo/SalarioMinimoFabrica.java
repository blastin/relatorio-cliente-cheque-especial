package dominio.salario.minimo;

public final class SalarioMinimoFabrica {

    private SalarioMinimoFabrica() {

    }

    public static SalarioMinimoGateway construir(final SalarioMinimoGateway salarioMinimoGateway, final SalarioMinimoCorrecaoGateway salarioMinimoCorrecaoGateway) {
        return new CasoDeUsoCorrecaoSalarioMinimo(salarioMinimoGateway, salarioMinimoCorrecaoGateway);
    }

    public static SalarioMinimoGateway construirParaTempoReal(final SalarioMinimoGateway salarioMinimoGateway, final SalarioMinimoCorrecaoGateway salarioMinimoCorrecaoGateway) {
        return new CasoDeUsoCorrecaoSalarioMinimoEmTempoReal(salarioMinimoGateway, salarioMinimoCorrecaoGateway);
    }

}
