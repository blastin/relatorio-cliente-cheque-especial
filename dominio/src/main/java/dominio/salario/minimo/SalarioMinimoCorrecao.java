package dominio.salario.minimo;

import java.math.BigDecimal;

public interface SalarioMinimoCorrecao {

    BigDecimal valor();

}
