package dominio.salario.minimo;

import java.math.BigDecimal;

@FunctionalInterface
public interface SalarioMinimo {

    BigDecimal valor();

}
