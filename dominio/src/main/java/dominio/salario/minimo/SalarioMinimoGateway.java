package dominio.salario.minimo;

@FunctionalInterface
public interface SalarioMinimoGateway {

    SalarioMinimo salarioMinimo();

}
