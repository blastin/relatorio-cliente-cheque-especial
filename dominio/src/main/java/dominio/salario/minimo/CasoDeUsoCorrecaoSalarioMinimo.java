package dominio.salario.minimo;

import java.math.BigDecimal;
import java.math.RoundingMode;

final class CasoDeUsoCorrecaoSalarioMinimo implements SalarioMinimoGateway {

    private final SalarioMinimoGateway salarioMinimoGateway;

    private final SalarioMinimoCorrecaoGateway salarioMinimoCorrecaoGateway;

    CasoDeUsoCorrecaoSalarioMinimo(
            final SalarioMinimoGateway salarioMinimoGateway,
            final SalarioMinimoCorrecaoGateway salarioMinimoCorrecaoGateway) {
        this.salarioMinimoGateway = salarioMinimoGateway;
        this.salarioMinimoCorrecaoGateway = salarioMinimoCorrecaoGateway;
    }

    @Override
    public SalarioMinimo salarioMinimo() {

        final var salarioMinimo = salarioMinimoGateway.salarioMinimo().valor();

        final var porcentagem =
                BigDecimal.ONE.subtract(salarioMinimoCorrecaoGateway.correcao().valor().divide(BigDecimal.valueOf(100), RoundingMode.CEILING));

        final var salarioMinimoCorrigido = salarioMinimo.multiply(porcentagem);

        return () -> salarioMinimoCorrigido;

    }
}
