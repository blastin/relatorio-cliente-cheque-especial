package dominio.salario.minimo;

@FunctionalInterface
public interface SalarioMinimoCorrecaoGateway {

    SalarioMinimoCorrecao correcao();

}
