package dominio.relatorio;

import java.util.stream.Stream;

@FunctionalInterface
public interface Relatorio<T, E> {

    E processar(Stream<? extends T> stream);

}
