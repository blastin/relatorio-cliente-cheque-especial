package dominio.cheque.especial.relatorio;

import dominio.cheque.especial.CasoDeUsoChequeEspecial;

public final class CasoDeUsoRelatorioChequeEspecialFabrica {

    private CasoDeUsoRelatorioChequeEspecialFabrica() {

    }

    public static CasoDeUsoRelatorioChequeEspecial construir(final CasoDeUsoChequeEspecial casoDeUsoChequeEspecial) {
        return new CasoDeUsoRelatorioChequeEspecialImpl(casoDeUsoChequeEspecial);
    }

}
