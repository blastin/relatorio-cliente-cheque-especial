package dominio.cheque.especial.relatorio;

import dominio.cheque.especial.CasoDeUsoChequeEspecial;
import dominio.cliente.Cliente;
import dominio.relatorio.Relatorio;

final class CasoDeUsoRelatorioChequeEspecialImpl implements CasoDeUsoRelatorioChequeEspecial {

    private final CasoDeUsoChequeEspecial casoDeUsoChequeEspecial;

    CasoDeUsoRelatorioChequeEspecialImpl(final CasoDeUsoChequeEspecial casoDeUsoChequeEspecial) {
        this.casoDeUsoChequeEspecial = casoDeUsoChequeEspecial;
    }

    @Override
    public <E> E obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(final Relatorio<Cliente, ? extends E> relatorio) {

        final var clientes = casoDeUsoChequeEspecial.obterClientesComChequeEspecialAcimalDoSalarioMinimo();

        return relatorio.processar(clientes);

    }

}
