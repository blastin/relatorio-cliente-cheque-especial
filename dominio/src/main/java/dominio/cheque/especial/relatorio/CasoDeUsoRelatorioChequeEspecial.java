package dominio.cheque.especial.relatorio;

import dominio.cliente.Cliente;
import dominio.relatorio.Relatorio;

@FunctionalInterface
public interface CasoDeUsoRelatorioChequeEspecial {

    <E> E obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(final Relatorio<Cliente, ? extends E> relatorio);

}
