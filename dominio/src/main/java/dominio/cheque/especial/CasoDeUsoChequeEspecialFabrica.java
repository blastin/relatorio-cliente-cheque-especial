package dominio.cheque.especial;

import dominio.cliente.ClientesGateway;
import dominio.salario.minimo.SalarioMinimoGateway;

public final class CasoDeUsoChequeEspecialFabrica {

    private CasoDeUsoChequeEspecialFabrica() {
    }

    public static CasoDeUsoChequeEspecial construir(final ClientesGateway clientesGateway, final SalarioMinimoGateway salarioMinimoGateway) {
        return new CasoDeUsoChequeEspecialImpl(clientesGateway, salarioMinimoGateway);
    }

}
