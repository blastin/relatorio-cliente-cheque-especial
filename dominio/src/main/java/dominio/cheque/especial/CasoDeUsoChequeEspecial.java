package dominio.cheque.especial;

import dominio.cliente.Cliente;

import java.util.stream.Stream;

@FunctionalInterface
public interface CasoDeUsoChequeEspecial {

    Stream<Cliente> obterClientesComChequeEspecialAcimalDoSalarioMinimo();

}
