package dominio.cheque.especial;

import dominio.cliente.Cliente;
import dominio.cliente.ClientesGateway;
import dominio.salario.minimo.SalarioMinimo;
import dominio.salario.minimo.SalarioMinimoGateway;

import java.util.stream.Stream;

final class CasoDeUsoChequeEspecialImpl implements CasoDeUsoChequeEspecial {

    private final ClientesGateway clientesGateway;

    private final SalarioMinimoGateway salarioMinimoGateway;

    CasoDeUsoChequeEspecialImpl(final ClientesGateway clientesGateway, final SalarioMinimoGateway salarioMinimoGateway) {
        this.clientesGateway = clientesGateway;
        this.salarioMinimoGateway = salarioMinimoGateway;
    }

    @Override
    public Stream<Cliente> obterClientesComChequeEspecialAcimalDoSalarioMinimo() {

        final var salarioMinimo = salarioMinimoGateway.salarioMinimo();

        return clientesGateway
                .clientes()
                .filter(cliente -> chequeEspecialAcimaSalarioMinimo(cliente, salarioMinimo));

    }

    private boolean chequeEspecialAcimaSalarioMinimo(final Cliente cliente, final SalarioMinimo salarioMinimo) {
        return cliente.chequeEspecial().compareTo(salarioMinimo.valor()) > 0;
    }

}
