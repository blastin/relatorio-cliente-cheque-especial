package dominio.cliente;

import java.util.stream.Stream;

@FunctionalInterface
public interface ClientesGateway {

    Stream<Cliente> clientes();

}
