package dominio.cliente;

import java.math.BigDecimal;

public interface Cliente {

    int id();

    BigDecimal chequeEspecial();

}
