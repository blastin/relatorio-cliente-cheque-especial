package dominio.cliente.relatorio;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dominio.cliente.Cliente;
import dominio.relatorio.Relatorio;

import java.util.stream.Stream;

final class RelatorioJSON implements Relatorio<Cliente, String> {

    private final Gson gson;

    RelatorioJSON() {
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    @Override
    public String processar(final Stream<? extends Cliente> stream) {
        return gson.toJson(stream.toArray());
    }

}
