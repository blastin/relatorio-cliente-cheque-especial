package dominio.cliente.relatorio;

import dominio.cliente.Cliente;
import dominio.relatorio.Relatorio;

import java.util.stream.Collectors;
import java.util.stream.Stream;

final class RelatorioXML implements Relatorio<Cliente, String> {

    @Override
    public String processar(final Stream<? extends Cliente> stream) {

        final String intermediario = stream
                .map(cliente -> "   <cliente id=\"" + cliente.id() + "\">\n       <cheque-especial>" + cliente.chequeEspecial() + "</cheque-especial>\n   </cliente>\n")
                .collect(Collectors.joining());

        return "<?xml version=\"1.0\"?>\n<Clientes>\n" + intermediario + "</Clientes>\n";

    }

}
