package dominio.cliente.relatorio;

import dominio.cliente.Cliente;
import dominio.relatorio.Relatorio;

public final class RelatorioFabrica {

    private RelatorioFabrica() {
    }

    public static Relatorio<Cliente, String> csv() {
        return new RelatorioSV(",");
    }

    public static Relatorio<Cliente, String> tsv() {
        return new RelatorioSV("\t");
    }

    public static Relatorio<Cliente, String> xml() {
        return new RelatorioXML();
    }

    public static Relatorio<Cliente, String> json() {
        return new RelatorioJSON();
    }

}
