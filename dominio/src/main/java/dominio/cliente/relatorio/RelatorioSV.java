package dominio.cliente.relatorio;

import dominio.cliente.Cliente;
import dominio.relatorio.Relatorio;

import java.util.stream.Collectors;
import java.util.stream.Stream;

final class RelatorioSV implements Relatorio<Cliente, String> {

    private final String delimitador;

    RelatorioSV(final String delimitador) {
        this.delimitador = delimitador;
    }

    @Override
    public String processar(final Stream<? extends Cliente> stream) {

        return stream
                .map(cliente -> cliente.id() + delimitador + cliente.chequeEspecial() + "\n")
                .collect(Collectors.joining());
        
    }

}
