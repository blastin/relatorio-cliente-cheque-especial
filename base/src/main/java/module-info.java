module base {
    exports base.salario;
    exports base.cliente;
    requires transitive infraestrutura;
    requires transitive dominio;
    requires java.net.http;
    requires java.sql;
    requires com.google.gson;
    requires org.slf4j;
}
