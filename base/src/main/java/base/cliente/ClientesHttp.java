package base.cliente;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import dominio.cliente.Cliente;
import infraestrutura.http.HttpComponente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.stream.Stream;

final class ClientesHttp extends ClientesAbstract {

    private static final Logger LOGGER = LoggerFactory.getLogger(ClientesHttp.class);

    private final HttpComponente httpComponente;

    private final Gson gson;

    private final String url;

    ClientesHttp(final HttpComponente httpComponente, final String url) {
        this.httpComponente = httpComponente;
        this.url = url;
        gson = new GsonBuilder().create();
    }

    @Override
    protected Stream<Cliente> obterClientes() {

        try {
            return Stream.of(gson.fromJson(httpComponente.verboGet(url).corpo(), ClienteJson[].class));
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return null;
    }

    public static final class ClienteJson implements Cliente {

        @SerializedName("id")
        public int identificador;

        @SerializedName("chequeEspecial")
        public BigDecimal valor;

        @Override
        public int id() {
            return identificador;
        }

        @Override
        public BigDecimal chequeEspecial() {
            return valor;
        }

    }

}
