package base.cliente;

import dominio.cliente.Cliente;
import infraestrutura.database.BancoDeDadosComponente;
import infraestrutura.database.BancoDeDadosException;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.stream.Stream;

final class ClientesBancoDeDados extends ClientesAbstract {

    private final BancoDeDadosComponente bancoDeDadosComponente;

    ClientesBancoDeDados(final BancoDeDadosComponente bancoDeDadosComponente) {
        this.bancoDeDadosComponente = bancoDeDadosComponente;
    }

    @Override
    protected Stream<Cliente> obterClientes() {

        try {
            return bancoDeDadosComponente.selecionar("select * from cliente", ClienteDatabase::new);
        } catch (BancoDeDadosException e) {
        }

        return null;
    }

    public static final class ClienteDatabase implements Cliente {

        public final int identificador;

        public final BigDecimal valor;

        private ClienteDatabase(final ResultSet resultSet) throws SQLException {
            identificador = resultSet.getInt(1);
            valor = resultSet.getBigDecimal(2);
        }

        @Override
        public int id() {
            return identificador;
        }

        @Override
        public BigDecimal chequeEspecial() {
            return valor;
        }

    }

}
