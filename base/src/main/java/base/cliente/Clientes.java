package base.cliente;

import dominio.cliente.ClientesGateway;
import infraestrutura.database.BancoDeDadosComponente;
import infraestrutura.http.HttpComponente;

public final class Clientes {

    private ClientesAbstract primeiraInstancia;

    private ClientesAbstract ultimaInstancia;

    public Clientes tenteComBancoDeDados(final BancoDeDadosComponente bancoDeDadosComponente) {

        final var bancoDeDados = new ClientesBancoDeDados(bancoDeDadosComponente);

        return inserirInstancia(bancoDeDados);

    }

    public Clientes tenteComHttp(final HttpComponente httpComponente, final String url) {

        final var http = new ClientesHttp(httpComponente, url);

        return inserirInstancia(http);

    }

    private Clientes inserirInstancia(final ClientesAbstract clientesAbstract) {

        clientesAbstract.adicionarProximo(null);

        if (primeiraInstancia != null) {
            ultimaInstancia.adicionarProximo(clientesAbstract);
        } else {
            primeiraInstancia = clientesAbstract;
        }

        ultimaInstancia = clientesAbstract;

        return this;

    }

    public ClientesGateway builder() {
        return primeiraInstancia;
    }

}
