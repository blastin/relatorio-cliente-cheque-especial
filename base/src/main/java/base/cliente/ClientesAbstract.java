package base.cliente;

import dominio.cliente.Cliente;
import dominio.cliente.ClientesGateway;

import java.util.Optional;
import java.util.stream.Stream;

abstract class ClientesAbstract implements ClientesGateway {

    private ClientesAbstract proximo;

    protected ClientesAbstract() {
        proximo = null;
    }

    final void adicionarProximo(final ClientesAbstract proximo) {
        this.proximo = Optional.ofNullable(proximo).orElse(new ClientesNulo());
    }

    @Override
    public final Stream<Cliente> clientes() {

        final Stream<Cliente> clienteStream = obterClientes();

        if (clienteStream != null) {
            return clienteStream;
        }

        return proximo.clientes();

    }

    protected abstract Stream<Cliente> obterClientes();

    private static final class ClientesNulo extends ClientesAbstract {

        protected ClientesNulo() {
            super();
        }

        @Override
        protected Stream<Cliente> obterClientes() {
            return Stream.empty();
        }

    }
}
