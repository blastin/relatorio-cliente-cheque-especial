package base.salario;

import dominio.salario.minimo.SalarioMinimoCorrecao;
import dominio.salario.minimo.SalarioMinimoCorrecaoGateway;

import java.math.BigDecimal;
import java.util.Optional;

abstract class SalarioMinimoCorrecaoAbstract implements SalarioMinimoCorrecaoGateway {

    private final SalarioMinimoCorrecaoAbstract proximo;

    protected SalarioMinimoCorrecaoAbstract(final SalarioMinimoCorrecaoAbstract proximo) {
        this.proximo = Optional.ofNullable(proximo).orElse(new SalarioMinimoCorrecaoNulo());
    }

    private SalarioMinimoCorrecaoAbstract() {
        proximo = null;
    }

    @Override
    public SalarioMinimoCorrecao correcao() {

        final SalarioMinimoCorrecao salarioMinimoCorrecao = obterSalarioMinimoCorrecao();

        if (salarioMinimoCorrecao != null) {
            return salarioMinimoCorrecao;
        }

        return proximo.correcao();

    }

    protected abstract SalarioMinimoCorrecao obterSalarioMinimoCorrecao();

    public static class SalarioMinimoCorrecaoNulo extends SalarioMinimoCorrecaoAbstract {

        private SalarioMinimoCorrecaoNulo() {
            super();
        }

        @Override
        protected SalarioMinimoCorrecao obterSalarioMinimoCorrecao() {
            return () -> BigDecimal.valueOf(3.67);
        }

    }
}
