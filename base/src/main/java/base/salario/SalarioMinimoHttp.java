package base.salario;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import dominio.salario.minimo.SalarioMinimo;
import infraestrutura.http.HttpComponente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public final class SalarioMinimoHttp extends SalarioMinimoAbstract {

    private static final Logger LOGGER = LoggerFactory.getLogger(SalarioMinimoHttp.class);

    private final Gson gson;

    private final HttpComponente httpComponente;

    private final String url;

    public SalarioMinimoHttp(final HttpComponente httpComponente, final String url) {
        super(null);
        this.httpComponente = httpComponente;
        this.url = url;
        gson = new GsonBuilder().create();
    }

    @Override
    protected SalarioMinimo obterSalarioMinimo() {

        try {
            return gson.fromJson(httpComponente.verboGet(url).corpo(), SalarioMinimoJsonAdaptador.class);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return null;

    }

    public static final class SalarioMinimoJsonAdaptador implements SalarioMinimo {

        @SerializedName("valorSalarioMinimo")
        public Double valorSalarioMinimo;

        @Override
        public BigDecimal valor() {
            return BigDecimal.valueOf(valorSalarioMinimo);
        }

    }

}
