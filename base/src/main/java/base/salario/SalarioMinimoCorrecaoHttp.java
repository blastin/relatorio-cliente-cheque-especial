package base.salario;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;
import dominio.salario.minimo.SalarioMinimoCorrecao;
import infraestrutura.http.HttpComponente;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public final class SalarioMinimoCorrecaoHttp extends SalarioMinimoCorrecaoAbstract {

    private static final Logger LOGGER = LoggerFactory.getLogger(SalarioMinimoCorrecaoHttp.class);

    private final Gson gson;

    private final HttpComponente httpComponente;

    private final String url;

    public SalarioMinimoCorrecaoHttp(final HttpComponente httpComponente, final String url) {
        super(null);
        this.httpComponente = httpComponente;
        this.url = url;
        gson = new GsonBuilder().create();
    }

    @Override
    protected SalarioMinimoCorrecao obterSalarioMinimoCorrecao() {

        try {
            return gson.fromJson(httpComponente.verboGet(url).corpo(), SalarioMinimoCorrecaoJson.class);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }

        return null;

    }

    public static final class SalarioMinimoCorrecaoJson implements SalarioMinimoCorrecao {

        @SerializedName("correcao")
        public Double correcao;

        @Override
        public BigDecimal valor() {
            return BigDecimal.valueOf(correcao);
        }

    }

}
