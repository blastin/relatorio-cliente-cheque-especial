package base.salario;

import dominio.salario.minimo.SalarioMinimo;
import dominio.salario.minimo.SalarioMinimoGateway;

import java.math.BigDecimal;
import java.util.Optional;

abstract class SalarioMinimoAbstract implements SalarioMinimoGateway {

    private final SalarioMinimoAbstract proximo;

    protected SalarioMinimoAbstract(final SalarioMinimoAbstract proximo) {
        this.proximo = Optional.ofNullable(proximo).orElse(new SalarioMinimoNulo());
    }

    private SalarioMinimoAbstract() {
        proximo = null;
    }

    @Override
    public SalarioMinimo salarioMinimo() {

        final SalarioMinimo salarioMinimo = obterSalarioMinimo();

        if (salarioMinimo != null) {
            return salarioMinimo;
        }

        return proximo.salarioMinimo();

    }

    protected abstract SalarioMinimo obterSalarioMinimo();

    public static class SalarioMinimoNulo extends SalarioMinimoAbstract {

        private SalarioMinimoNulo() {
            super();
        }

        @Override
        protected SalarioMinimo obterSalarioMinimo() {
            return () -> BigDecimal.valueOf(5000);
        }

    }
}
