package base;

import base.cliente.Clientes;
import base.salario.SalarioMinimoCorrecaoHttp;
import base.salario.SalarioMinimoHttp;
import dominio.cheque.especial.CasoDeUsoChequeEspecialFabrica;
import dominio.cheque.especial.relatorio.CasoDeUsoRelatorioChequeEspecial;
import dominio.cheque.especial.relatorio.CasoDeUsoRelatorioChequeEspecialFabrica;
import dominio.cliente.Cliente;
import dominio.cliente.relatorio.RelatorioFabrica;
import dominio.relatorio.Relatorio;
import dominio.salario.minimo.SalarioMinimoFabrica;
import infraestrutura.database.BancoDeDadosComponenteFabrica;
import infraestrutura.http.HttpComponenteFabrica;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.http.HttpClient;
import java.sql.DriverManager;

import static java.util.concurrent.TimeUnit.NANOSECONDS;

public final class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        LOGGER.info("Sistema para exportação de relatório de clientes");

        LOGGER.info("Fabricando Plugins necessários ");

        final var httpComponente = HttpComponenteFabrica.construir(HttpClient.newBuilder().build());

        final var bancoDeDadosComponente = BancoDeDadosComponenteFabrica.construir(() -> DriverManager.getConnection("jdbc:mariadb://localhost:3306/dev-database", "dev", "dev"));

        final var clientesGateway =
                new Clientes()
                        .tenteComBancoDeDados(bancoDeDadosComponente)
                        .tenteComHttp(httpComponente, "https://clientes.free.beeceptor.com/clientes")
                        .tenteComHttp(httpComponente, "http://www.mocky.io/v2/5e67a5ac3100005c00230b69")
                        .builder();

        final var salarioMinimoGateway = new SalarioMinimoHttp(httpComponente, "http://www.mocky.io/v2/5e65a32c31000063002399a6");

        final var salarioMinimoCorrecaoGateway = new SalarioMinimoCorrecaoHttp(httpComponente, "http://www.mocky.io/v2/5e65a35831000069002399a7");

        final var salarioMinimoGatewayFinal = SalarioMinimoFabrica.construir(salarioMinimoGateway, salarioMinimoCorrecaoGateway);

        final var casoDeUsoChequeEspecial = CasoDeUsoChequeEspecialFabrica.construir(clientesGateway, salarioMinimoGatewayFinal);

        final var casoDeUsoRelatorioChequeEspecial = CasoDeUsoRelatorioChequeEspecialFabrica.construir(casoDeUsoChequeEspecial);

        final var relatorioCliente = RelatorioFabrica.xml();

        LOGGER.info("Iniciando exportação de relatório");

        new Main().iniciar(casoDeUsoRelatorioChequeEspecial, relatorioCliente);

    }

    private void iniciar(final CasoDeUsoRelatorioChequeEspecial casoDeUsoRelatorioChequeEspecial, final Relatorio<Cliente, String> relatorioCliente) {

        final var startTime = System.nanoTime();

        final var relatorio = casoDeUsoRelatorioChequeEspecial
                .obterRelatorioInformativoDeClientesComChequeEspecialAcimaSalarioMinimo(relatorioCliente);

        LOGGER.info("\nrelatorio ->\n{}", relatorio);

        final var endTime = System.nanoTime() - startTime;

        final var segundos = NANOSECONDS.toSeconds(endTime);

        LOGGER.info("tempo total {} segundos ", segundos);

    }

}
